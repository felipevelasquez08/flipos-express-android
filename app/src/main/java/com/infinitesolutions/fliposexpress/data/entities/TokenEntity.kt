package com.infinitesolutions.fliposexpress.data.entities

data class TokenEntity(val key: String, val user: UserEntity)