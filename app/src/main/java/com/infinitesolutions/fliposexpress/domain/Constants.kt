package com.infinitesolutions.fliposexpress.domain

class Constants {
    companion object {

        const val ERROR =
            "Lo sentimos, estamos presentando problemas internos. Intenta nuevamente"
        const val TAG = "tag_message"

        const val KEY = "key"
        const val USER = "user"
        const val ID = "id"
        const val EMAIL = "email"
        const val USERNAME = "username"
        const val PASSWORD = "password"
        const val COST = "cost"
        const val ORDER_COST = "cost_order"
        const val DESCRIPTION = "description"
        const val USER_ID = "user_id"
        const val DATE = "date"
        const val FINISH = "finish"
    }
}