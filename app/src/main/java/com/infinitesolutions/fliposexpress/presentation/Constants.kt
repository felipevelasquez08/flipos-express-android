package com.infinitesolutions.fliposexpress.presentation

class Constants {
    companion object {
        const val TYPE = "type"
        const val ADD = "add"
        const val UPDATE = "update"
        const val ORDER = "order"
    }
}